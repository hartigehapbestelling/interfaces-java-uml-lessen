/**
 * Avans IVH5 Proftaak API
 * 
 * This file is part of the IVH5 proftaak API.
 * 
 */
package edu.avans.ivh5.fysiotherapeut.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import edu.avans.ivh5.fysiotherapeut.Treatment;

/**
 * Avans IVH5 Fysio interface
 * 
 * @author Robin Schellius
 *
 */
public interface FysioServiceIF extends Remote {
    
    
    //behandelgegevens
    
    /**
     * Get treatment data
     * */
    List<String> getTreatments(String name) throws RemoteException;
    
    /**
     * Get treatment data
     * 

     * TODO: change to treatment object  
     * */
    Treatment getTreatment(int id) throws RemoteException;
    
    /**
     * Get treatment data
     * TODO: change to treatment object  
     * */
    List<Treatment> getTreatments(String name, int limit) throws RemoteException;
    
    
}

