/**
 * Avans IVH5 Proftaak API
 * 
 * This file is part of the IVH5 proftaak API.
 * 
 */
package edu.avans.ivh5.verzekeraar.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Avans IVH5 Verzekeraar interface
 * 
 * Give customer data to a physiotherapist
 * 
 * @author Robin Schellius
 *
 */
public interface VerzekeraarServiceIF extends Remote {
		
    
    //klant data
    /**
     * Dummy setter method - will be deleted at a later moment.
     * 
     * @param valueOne A value
     * @param valueTwo Another value
     */
    void setSomeValue(String valueOne, int valueTwo);
	
	/**
	 * Dummy getter method - will be deleted at a later moment.
	 * 
	 * @return The value
	 * @throws RemoteException
	 */
    String getSomeValue(int someID) throws RemoteException;
}

